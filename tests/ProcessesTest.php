<?php

use SergeyM\Processes\Exceptions\PIDCanOnlyBeAnIntegerException;
use PHPUnit\Framework\TestCase;
use SergeyM\Processes\Processes;
use Symfony\Component\Process\Process;

class ProcessesTest extends TestCase
{
    /** @var int */
    private $pid = 0;

    /**
     * @dataProvider processesArgumentsProvider
     * @param null|bool $all
     * @param null|bool $multi
     * @throws PIDCanOnlyBeAnIntegerException
     */
    public function testProcessesOnUnix(?bool $all, ?bool $multi): void
    {
        if ('\\' === DIRECTORY_SEPARATOR) {
            self::markTestSkipped('This test runs only on Unix');
        }

        $process = new Process(['tests/bin/while']);

        $process->start();
        $this->pid = $process->getPid();

        self::assertTrue($process->isStarted());
        self::assertFalse($process->isTerminated());

        self::assertGreaterThan(0, $this->pid);

        $processes = new Processes($all, $multi);

        self::assertFalse($processes->exists(null));
        self::assertTrue($processes->exists($this->pid));

        $processInformation = $processes[$this->pid];

        self::assertArrayHasKey(Processes::PID, $processInformation);
        self::assertIsInt($processInformation[Processes::PID]);
        self::assertGreaterThan(0, $processInformation[Processes::PID], 'PID');
        self::assertArrayHasKey(Processes::PPID, $processInformation);
        self::assertIsInt($processInformation[Processes::PPID]);
        self::assertGreaterThanOrEqual(0, $processInformation[Processes::PPID], 'PPID');
        self::assertArrayHasKey(Processes::NAME, $processInformation);
        self::assertIsString($processInformation[Processes::NAME]);
        self::assertGreaterThan(0, strlen($processInformation[Processes::NAME]), 'Name length');

        self::assertArrayHasKey(Processes::UID, $processInformation);
        self::assertIsInt($processInformation[Processes::UID]);
        self::assertGreaterThan(0, $processInformation[Processes::PID], 'UID');
        self::assertArrayHasKey(Processes::CPU, $processInformation);
        self::assertIsFloat($processInformation[Processes::CPU]);
        self::assertArrayHasKey(Processes::MEMORY, $processInformation);
        self::assertIsFloat($processInformation[Processes::MEMORY]);
        self::assertArrayHasKey(Processes::CMD, $processInformation);
        self::assertIsString($processInformation[Processes::CMD]);
        // in busybox environment COMMAND is always empty
        if ($processes->getResultType() !== Processes::BUSY_BOX_RESULT) {
            self::assertGreaterThan(0, strlen($processInformation[Processes::CMD]), 'Command length');
        }

        $process->stop();

        self::assertNull($process->getPid());
        self::assertFalse($process->isRunning());
        self::assertTrue($process->isTerminated());

        self::assertGreaterThanOrEqual(count($processes->rescan()), count($processes));

        $this->expectException(PIDCanOnlyBeAnIntegerException::class);
        $processes->exists('string');
    }

    public function processesArgumentsProvider(): array
    {
        return [
            [null, null],
            [true, false],
            [true, true],
            [false, false],
            [false, true],
        ];
    }

    /**
     * @throws PIDCanOnlyBeAnIntegerException
     */
    public function testProcessesOnWindows(): void
    {
        if ('\\' !== DIRECTORY_SEPARATOR) {
            self::markTestSkipped('This test runs only on Windows');
        }

        $process = new Process(['tests/bin/while.exe']);

        $process->start();
        $this->pid = $process->getPid();

        self::assertTrue($process->isStarted());
        self::assertFalse($process->isTerminated());

        self::assertGreaterThan(0, $this->pid);

        $processes = new Processes(true);

        self::assertFalse($processes->exists(null));
        self::assertTrue($processes->exists($this->pid));

        $processInformation = $processes->get()[$this->pid];

        self::assertArrayHasKey(Processes::PID, $processInformation);
        self::assertIsInt($processInformation[Processes::PID]);
        self::assertArrayHasKey(Processes::PPID, $processInformation);
        self::assertIsInt($processInformation[Processes::PPID]);
        self::assertArrayHasKey(Processes::NAME, $processInformation);
        self::assertIsString($processInformation[Processes::NAME]);

        $process->stop();

        self::assertNull($process->getPid());
        self::assertFalse($process->isRunning());
        self::assertTrue($process->isTerminated());
    }
}
