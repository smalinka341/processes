# SergeyM\Processes

This package used for getting list of running processes on Windows or Unix-like systems

> Thanks to [fastlist](https://github.com/MarkTiedemann/fastlist) for processes on Windows.

## Usage

```php
use SergeyM\Processes\Processes;

// some PID, integer
$pid = 1;
// get all processes except both session leaders, default false
$all = true;

$processes = new Processes($all);
$exists = $processes->exists($pid); // return true of false
$processes = $processes->get(); // return array of processes where key is PID

// for rescanning processes, call rescan() method
$freshProcesses = $processes->rescan()->get();
```

## Structure of processes data

#### For windows
```json
{
  "PID": {
    "pid": "integer",
    "ppid": "integer",
    "name": "string"
  }
}
```

#### For unix-like systems
```json
{
  "PID": {
    "pid": "integer",
    "ppid": "integer",
    "name": "string",
    "uid": "integer",
    "cpu": "float",
    "memory": "float",
    "cmd": "string"
  }
}
```

## Testing
```sh
composer test
```
